#!/usr/bin/ruby

puts "=====ruby each iterator======"
ary = [1,2,3,4,5]
ary.each do |i|
   puts i
end

puts "=====ruby collect iterator======"
a = [1,2,3,4,5]
b = Array.new
b = a.collect
p b
a = [1,2,3,4,5]
b = a.collect{|x| 10*x}
puts b