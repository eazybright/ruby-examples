#!/usr/bin/ruby -w

$LOAD_PATH << './modules'

require "mail_config"
require 'net/smtp'

message = <<MESSAGE_END
From: Private Person <me@fromdomain.com>
To: A Test User <test@todomain.com>
MIME-Version: 1.0
Content-type: text/html
Subject: SMTP e-mail test

This is an e-mail message to be sent in HTML format<br>

<b>This is HTML message.</b>
<h1>This is headline.</h1>
MESSAGE_END

Net::SMTP.start( 
    MailConfig::HOST, 
    MailConfig::PORT, 
    MailConfig::DOMAIN, 
    MailConfig::USERNAME, 
    MailConfig::PASSWORD, 
    :cram_md5
) do |smtp|
    smtp.send_message message, 'me@fromdomain.com', 'test@todomain.com'
end