#!/usr/bin/ruby -w

$LOAD_PATH << './modules'

require "mail_config"
require 'net/smtp'

message = <<MESSAGE_END
From: Private Person <me@fromdomain.com>
To: A Test User <test@todomain.com>
Subject: SMTP e-mail test

This is a test e-mail message.
MESSAGE_END

begin
    puts "<sending email using localhost...>"
    Net::SMTP.start('localhost') do |smtp|
        smtp.send_message message, 'me@fromdomain.com', 'test@todomain.com'
    end

rescue Exception => e
    puts "Error: #{e}"

ensure
    puts "<retrying with mailtrap domain...>"
    Net::SMTP.start(
        MailConfig::HOST, 
        MailConfig::PORT, 
        MailConfig::DOMAIN, 
        MailConfig::USERNAME, 
        MailConfig::PASSWORD, 
        :cram_md5
    ) do |smtp|
        smtp.send_message message, 'me@fromdomain.com', 'test@todomain.com'
    end
    puts "email sent"
end
