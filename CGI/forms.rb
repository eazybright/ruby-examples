#!/usr/bin/ruby

require "cgi"

# escaping strings
puts CGI.escape("Zara Ali/A Sweet & Sour Girl")
puts CGI.escapeHTML('<h1>Zara Ali/A Sweet & Sour Girl</h1>')

cgi = CGI.new("html4")
cgi.out {
   cgi.html {
      cgi.head { "\n"+cgi.title{"This Is a Test"} } +
      cgi.body { "\n"+
         cgi.form {"\n"+
            cgi.hr +
            cgi.h1 { "A Form: " } + "\n"+
            cgi.textarea("get_text") +"\n"+
            cgi.br +
            cgi.submit
         }
      }
   }
}