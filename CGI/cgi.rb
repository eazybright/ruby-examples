#!/usr/bin/ruby

require 'cgi'
cgi = CGI.new

# form processing: example of URL /cgi-bin/test.cgi?FirstName = Zara&LastName = Ali
cgi['FirstName'] # =>  ["Zara"]
cgi['LastName']  # =>  ["Ali"] 

h = cgi.params  # =>  {"FirstName"=>["Zara"],"LastName"=>["Ali"]}
h['FirstName']  # =>  ["Zara"]
h['LastName']   # =>  ["Ali"]

cgi.keys # retrieve all keys

# puts cgi.header
# puts "<html><body>This is a test</body></html>"