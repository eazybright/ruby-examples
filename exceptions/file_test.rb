#!/usr/bin/ruby

begin
    file = open("/unexistant_file")
    if file
       puts "File opened successfully"
    end
rescue
    file = STDIN
end
print file, "==", STDIN, "\n"

# using retry statement
# begin
#     file = open("/unexistant_file")
#     if file
#        puts "File opened successfully"
#     end
# rescue
#     fname = "existant_file"
#     retry
# end

# using raise statement
begin  
    puts 'I am before the raise.'  
    raise 'An error has occurred.'  
    puts 'I am after the raise.'  
rescue  
    puts 'I am rescued.'  
end  
puts 'I am after the begin block.' 

begin  
    raise 'A test exception.'  
rescue Exception => e  
    puts e.message  
    puts e.backtrace.inspect  
ensure
    puts "Ensuring execution"
end 

# using else statement
begin
    puts "I'm not raising exception"
rescue Exception => e
    puts e.message
    puts e.backtrace.inspect
else
    puts "Congratulations-- no errors!"
ensure
    puts "Ensuring execution"
end