class FileSaveError < StandardError
    attr_reader :reason
    def initialize(reason)
       @reason = reason
    end
end

path = 'file.txt'

File.open(path, "w") do |file|
    begin
       # Write out the data ...
       p file
    rescue
       # Something went wrong!
       raise FileSaveError.new($!)
    end
end