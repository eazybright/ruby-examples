#!/usr/bin/ruby

# The gets Statement
puts "Enter a value :"
val = gets
puts val

# The putc statement
str = "Hello Ruby!"
putc str

# The print statement
print "Hello World"
print "Good Morning"

# The sysread Method
aFile = File.new("input.txt", "r")
if aFile
   content = aFile.sysread(20)
   puts content
else
   puts "Unable to open file!"
end

# The syswrite Method
aFile = File.new("input.txt", "r+")
if aFile
   aFile.syswrite("ABCDEF")
else
   puts "Unable to open file!"
end

# The each_byte method
if aFile
    aFile.syswrite("ABCDEF")
    aFile.each_byte {|ch| putc ch; putc ?. }
else
    puts "Unable to open file!"
end

puts "======readlines======="
arr = IO.readlines("input.txt")
arr.each.with_index(1) do |i, index|
    puts "line #{index}: #{i}"
end

IO.foreach("input.txt"){|block| puts block}

# Rename a file from input.txt to input2.txt
# File.rename( "input.txt", "input2.txt" )

# Delete file test2.txt
# File.delete("test2.txt")

# File::directory?( "/usr/local/bin" )