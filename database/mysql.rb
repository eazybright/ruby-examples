
#!/usr/bin/ruby

# Learn more on how to use mysql2 module at https://github.com/brianmario/mysql2

require 'mysql2'

client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "password")
client.query('SHOW DATABASES').each do |row|
    puts row
end