#!/usr/bin/ruby -w

# This didn't work because i was not able to install dbd-mysql module on my local machine
require "rubygems"
require "dbi"

begin
   # connect to the MySQL server
   dbh = DBI.connect("dbi:Mysql:reponse:localhost", "root", "password")
   # get server version string and display it
   row = dbh.select_one("SELECT VERSION()")
   puts "Server version: " + row[0]
rescue DBI::DatabaseError => e
   puts "An error occurred"
   puts "Error code:    #{e.err}"
   puts "Error message: #{e.errstr}"
ensure
   # disconnect from server
   dbh.disconnect if dbh
end
